package uk.co.jonniegrieve.funfacts2017;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class FunFactsActivity extends AppCompatActivity {
    public static final String TAG = FunFactsActivity.class.getSimpleName();

    //instantiate FactBook object
    private FactBook factBook = new FactBook();

    //instantiate ColorWheel object
    private ColorWheel colourWheel = new ColorWheel();

    //declare two fields or member variables.
    private TextView factTextView;
    private Button showFactButton;
    private RelativeLayout backgroundColours;

    String toast = "Toast";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fun_facts);

        //assign views from layout file to corresponding variables.
        factTextView = (TextView) findViewById(R.id.factTextView);
        showFactButton = (Button) findViewById(R.id.showFactButton);
        backgroundColours = (RelativeLayout) findViewById(R.id.backgroundColours);


        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String fact = factBook.getFacts();
                int  colour = colourWheel.getColor();
                factTextView.setText(fact);
                backgroundColours.setBackgroundColor(colour);
                showFactButton.setTextColor(colour);

            }
        };
        showFactButton.setOnClickListener(listener);

        Log.d(TAG, "New log from the onCreate method");
        //Toast.makeText(this, toast, Toast.LENGTH_LONG).show();
    }
}